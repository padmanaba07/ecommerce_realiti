import React, { Fragment } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Section from "./components/section";
import Product from "./components/product";
import Cart from "./components/cart";

import Header from "./components/layouts/header";
import Footer from "./components/layouts/footer";

import { DataProvider } from "./components/Context";

const App = () => {
  return (
    <DataProvider>
      <div className="app">
        <Router>
          <Header />
          <Section />
        </Router>
      </div>
    </DataProvider>
  );
};

export default App;
