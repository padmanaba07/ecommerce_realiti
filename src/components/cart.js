import React, { Component } from 'react';
import {Datacontext} from './Context'
import { Link } from 'react-router-dom';
import './css/cart.css'

export class cart extends Component {
  static contextType = Datacontext;

  componentDidMount(){
    this.context.getTotal()
  }
  render() {
    const {cart, reduction, increase, removeProduct, total} = this.context;
console.log(cart.length)
    if(cart.length === 0)
    {
      return <h2 style={{textAlign: 'center'}}>your Cart is Empty</h2>
    }
    else
    {
      return <>
    {
      cart.map(item => (
        <div className='details cart' key={item._id} >
              <img src ={item.src} alt="" />
                <div className='box'>
                    <div className='row'>
                      <h2>{item.title}</h2>
                      <span>${item.price * item.count}</span>
                    </div>
                    <p>{item.colors.map((color,index) => (
                        <button key={index} style={{background: color}}></button>
                    ))}</p>
                    <p>{item.description}</p>
                    <div className='amount'>
                        <button className='count' onClick={() => this.context.reduction(item._id)}> - </button>
                        <span>{item.count}</span>
                        <button className='count' onClick={() => this.context.increase(item._id)}> + </button>

                    </div>
                </div>
                <div className='delete' onClick={() => this.context.removeProduct(item._id)}>X</div>
              </div>
      ))
    }

    <div className='total'>
      <Link to='/payment'>Payment
      </Link>
      <h3>{total}</h3>
    </div>
    </>
    }
    
  }
}

export default cart;
