import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Datacontext } from "./Context";
import "./css/product.css";

export class product extends Component {
  static contextType = Datacontext;

  render() {
    const { products } = this.context;
    return (
      <div id="product">
        {products.map((product) => (
          <div className="card" key={product._id}>
            <Link to={`/product/${product._id}`}>
              <img src={product.src} alt="No Image" />
            </Link>
            <div className="content">
              <h3>
                <Link to={`/product/${product._id}`}>{product.title}</Link>
              </h3>
              <span>${product.price}</span>
              <button onClick={() => this.context.addCart(product._id)}>
                Add to Cart
              </button>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default product;
