import React, { Component } from "react";
import Product from "./product";
import Details from "./details";
import Cart from "./cart";
import Payment from "./payment";

import { Route } from "react-router-dom";

const section = () => {
  return (
    <section>
      <Route exact path="/" component={Product} />
      <Route exact path="/product" component={Product} />
      <Route exact path="/product/:id" component={Details} />
      <Route exact path="/cart" component={Cart} />
      <Route exact path="/payment" component={Payment} />
    </section>
  );
};

export default section;
