import React, { Component } from 'react';
import {Datacontext} from './Context'
import { Link } from 'react-router-dom';
import './css/details.css'

export class details extends Component {

  static contextType = Datacontext;
  state ={
    product: []
  }

  getProduct = () => {
    if(this.props.match.params.id)
    {
      const res = this.context.products;
      const data = res.filter(item => {
        return item._id === this.props.match.params.id
      })
      this.setState({product: data})
    }
  }

  componentDidMount(){
    this.getProduct();
  }
  render() {
    const {product} =this.state
    const {addCart} = this.context
    return(
        <>
        {
          product.map(item => (
            <div className='details' key={item._id} >
                  <img src ={item.src} alt="" />
                    <div className='box'>
                        <div className='row'>
                          <h2>{item.title}</h2>
                          <span>${item.price}</span>
                        </div>
                        <p>{item.colors.map((color,index) => (
                            <button key={index} style={{background: color}}></button>
                        ))}</p>
                        <p>{item.description}</p>
                        <Link to="/cart" className='cart' onClick={() => addCart(item._id)}>
                        Add to Cart
                        </Link>
                    </div>
                  </div>
          ))
        }
        </>
  ) 
  }
}

export default details;
