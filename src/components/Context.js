import React, { Component } from "react";

export const Datacontext = React.createContext();

export class DataProvider extends Component {
  state = {
    products: [
      {
        _id: "1",
        title: "Iphone 6s",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059288/iphone11_zwvhfd.jpg",
        description: "Iphone X user Friendly",
        price: 50,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "2",
        title: "Iphone X",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059287/iphone6s_qnimzj.jpg",
        description: "Iphone X user Friendly",
        price: 70,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "3",
        title: "Iphone XI",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059287/iphone12_nzes6x.jpg",
        description: "Iphone X user Friendly",
        price: 9,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "4",
        title: "Iphone X",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059287/iphone13_tptac4.jpg",
        description: "Iphone X user Friendly",
        price: 99,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "5",
        title: "Galaxy S10",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059581/samsung-galaxy-s10-cardinal-red_ueadbm.jpg",
        description: "Samsung Galaxy S10",
        price: 59,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "6",
        title: "Galaxy S20",
        src: "https://picsum.photos/200",
        description: "Iphone X user Friendly",
        price: 98,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "7",
        title: "Galaxy S20",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059780/samsung-galaxy-s20-ultra_158314982170_pwoac7.jpg",
        description: "Samsung Galaxy S20",
        price: 59,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "8",
        title: "Mi 11X",
        src: "https://picsum.photos/200",
        description: "Iphone X user Friendly",
        price: 69,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "9",
        title: "Mi 11X",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059848/1626675876_01XiaomiMi11XProCoverImage_xywcq5.webp",
        description: "Mi 11X",
        price: 67,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "10",
        title: "Mi 11X ultra",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059912/Xiaomi_Mi11_Ultra_Launch_amazon_small_1618579630913_mrn6tf.webp",
        description: "Mi 11X Ultra",
        price: 78,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "11",
        title: "Realme 5",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059985/5114EtDzdkS._AC_SS450__vwxhyg.jpg",
        description: "Realme 5 Pro",
        price: 45,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
      {
        _id: "12",
        title: "Redmi Note 10 Pro",
        src: "https://res.cloudinary.com/appstar-applications/image/upload/v1644059985/5114EtDzdkS._AC_SS450__vwxhyg.jpg",
        description: "Redmi note 10 Pro",
        price: 34,
        colors: ["black", "rose", "gold"],
        count: 1,
      },
    ],
    cart: [],
    total: 0,
  };

  addCart = (id) => {
    const { products, cart } = this.state;
    const check = cart.every((item) => {
      return item._id !== id;
    });
    if (check) {
      const data = products.filter((product) => {
        return product._id === id;
      });
      this.setState({ cart: [...cart, ...data] });
    } else {
      alert("Item Already Added in the Cart");
    }
  };

  reduction = (id) => {
    const { cart } = this.state;
    cart.forEach((item) => {
      if (item._id === id) {
        item.count === 1 ? (item.count = 1) : (item.count -= 1);
      }
    });
    this.setState({ cart: cart });
    this.getTotal();
  };

  increase = (id) => {
    const { cart } = this.state;
    cart.forEach((item) => {
      if (item._id === id) {
        item.count += 1;
      }
    });
    this.setState({ cart: cart });
    this.getTotal();
  };

  removeProduct = (id) => {
    if (window.confirm("Do you want to delete the product")) {
      const { cart } = this.state;

      cart.forEach((item, index) => {
        if (item._id === id) {
          cart.splice(index, 1);
        }
      });
      this.setState({ cart: cart });
      this.getTotal();
    }
  };

  getTotal = () => {
    const { cart } = this.state;
    const res = cart.reduce((prev, item) => {
      return prev + item.price * item.count;
    }, 0);
    this.setState({ total: res });
  };
  render() {
    const { products, cart, total } = this.state;
    const { addCart, reduction, increase, removeProduct, getTotal } = this;
    return (
      <Datacontext.Provider
        value={{
          products,
          addCart,
          cart,
          reduction,
          increase,
          removeProduct,
          getTotal,
        }}
      >
        {this.props.children}
      </Datacontext.Provider>
    );
  }
}
